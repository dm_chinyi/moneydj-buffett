export function scrollSmoothToSection() {
  const navLinks = [...document.querySelectorAll('.navbar__link, .navbar-mobile__link')];

  navLinks.forEach(link => {
    link.addEventListener('click', (e) => {
      e.preventDefault();

      const target = document.querySelector(e.target.hash);
      const isMobile = e.target.classList.contains('navbar-mobile__link');
      const header = document.querySelector('.header');

      target.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });

      if (!isMobile) return;
      header.classList.remove('nav-is-open');
    })
  })
}

export function fixNavbarAtTop() {
  const scrolledDistance = window.pageYOffset || document.documentElement.scrollTop;
  const header = document.querySelector('.header');

  if (scrolledDistance > header.offsetHeight) {
    header.classList.add('is-fixed');
  } else {
    header.classList.remove('is-fixed');
  }
}
