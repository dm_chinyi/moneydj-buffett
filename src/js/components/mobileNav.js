
export default function toggleMobileNav() {
  const header = document.querySelector('.header');

  header.classList.toggle('nav-is-open');
}


