// 投資觀點：解決 More Button 固定等高置底
export default function setContentHeight() {
  const cards = [...document.querySelectorAll('.img-card__bottom')];
  let maxHeight = 0;

  cards.forEach(content => {
    const contentHeight = content.offsetHeight;

    if(contentHeight > maxHeight) {
      maxHeight = contentHeight;
    }
  })

  cards.forEach(content => {
    content.style.height = `${maxHeight}px`;
  })
}
