// Components
import toggleMobileNav from './js/components/mobileNav';
import { scrollSmoothToSection, fixNavbarAtTop } from './js/components/scroll';
import setContentHeight from './js/components/imgCard';
import './styles/index.scss';

// Plugins
import smoothscroll from 'smoothscroll-polyfill';

// Function Executions
const navBtn = document.querySelector('#js-mobile-nav');
navBtn.addEventListener('click', toggleMobileNav);

window.addEventListener('scroll', fixNavbarAtTop);

scrollSmoothToSection();
smoothscroll.polyfill();

setContentHeight();
