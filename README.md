# 人人都是巴菲特 MoneyDJ海外投資方案
本專案使用 NPM、Webpack、SCSS、ES6、Babel，開發前請確認電腦環境有安裝以下配置。

## 必備環境

> [Node.js](https://nodejs.org/)：安裝 Node.js 同時，也會自動安裝 NPM (套件管理工具)。

檢查 NPM 是否有安裝成功，可輸入以下指令：
```
npm -v
```
正確安裝完成會看到類似 6.10.0 的版本訊息

## 檔案目錄架構
```
├── config
├── src/
    └── images
    └── js
      └── components
        └── imgCard.js
        └── mobileNav.js
        └── scroll.js
    └── styles
      └── base
      └── components
      └── index.scss
      └── main.scss
    └── templates
      └── buffett.html
    index.js
├── dist/
├── package.json
```
- config：Webpack 設定檔
- src：開發用 (Development)
    - templates/buffett.html：主要 HTML 檔案
    - styles/main.scss：主要 SCSS 檔案
    - styles/components：元件的 SCSS 檔案
    - js/components: 元件的 JS 檔案
    - images：所有圖檔位置
- dist：線上發佈用 (Production)，Webpack 將所有檔案經過編譯、合併、壓縮等流程後自動產生的資料夾。
- package.json：所有專案套件描述檔

## 如何使用
根據 `package.json` 安裝專案需要的所有套件

```
npm install
```

## 開發

開發時輸入以下，會自動開啟 Webpack Dev Server，修改時會自動更新網頁。

```
npm run start
```

## 部署上線

開發完成後輸入以下指令，Webpack 會將所有檔案經過編譯、合併、壓縮等流程，自動產出 `dist` 資料夾，將 `dist` 裡所有檔案部署到線上即可。

```
npm run build
```

